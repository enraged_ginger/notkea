package com.enragedginger.notkea.modules

import com.enragedginger.notkea.repositories.InventoryRepository
import com.google.inject.AbstractModule

/**
 * @author Stephen Hopper.
 */
class RepositoryModule extends AbstractModule {
  override def configure(): Unit = {
    bind(classOf[InventoryRepository])
  }
}
