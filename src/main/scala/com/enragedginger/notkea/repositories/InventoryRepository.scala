package com.enragedginger.notkea.repositories

import java.io.File
import javax.inject.{Inject, Singleton}

import com.enragedginger.notkea.models.{InventoryEntry, Item}
import com.github.tototoshi.csv.CSVReader

import scala.collection.mutable

/**
 * Repository for reading the inventory from a file.
 * @author Stephen Hopper.
 */
@Singleton
class InventoryRepository @Inject()() {

  /**
   * Reads the inventory from the inventory file.
   * @return The inventory as a mutable map from item ids to inventory entries.
   */
  def readInventoryFromFile(): mutable.HashMap[Long, InventoryEntry] = {
    val inventoryMap = CSVReader.open(new File("src/main/resources/inventory.csv")).allWithHeaders().map {
      row =>
        val item = Item(
          id = row("id").toLong,
          name = row("name"),
          description = row("description"),
          price = row("price").toFloat
        )
        val quantity = row("quantity").toInt
        (item.id, InventoryEntry(quantity, item))
    }

    mutable.HashMap(inventoryMap: _*)
  }


}
