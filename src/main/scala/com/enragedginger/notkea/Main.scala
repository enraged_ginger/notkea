package com.enragedginger.notkea

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.pattern.ask
import akka.util.Timeout
import com.enragedginger.notkea.actors.Customer.{GetShoppingList, GoShopping, ShoppingList}
import com.enragedginger.notkea.actors.StoreManager.{GetComplaints, GetSalesReport, SalesReport}
import com.enragedginger.notkea.actors._
import com.enragedginger.notkea.akkaguice.GuiceAkkaExtension
import com.enragedginger.notkea.modules.MainModule
import com.google.inject.name.Names
import com.google.inject.{Guice, Key}

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.{Failure, Success}

/**
 * A main class to start up the Notkea store simulator.
 */
object Main extends App {

  implicit val timeout: Timeout = 120 seconds

  val customerCount = args.headOption.map(_.toInt).getOrElse(25)

  //create the actor system
  import scala.concurrent.ExecutionContext.Implicits.global
  val injector = Guice.createInjector(new MainModule())
  implicit val system = injector.getInstance(classOf[ActorSystem])

  //grab references to the actors we'll be interacting with
  val storeManager = injector.getInstance(Key.get(classOf[ActorRef], Names.named(StoreManager.name)))
  val customers = List.range(0, customerCount).map {
    index => system.actorOf(propsForActor(Customer.name))
  }

  //grab the shopping lists for the various customers
  val shoppingListsF = for {
    shoppingLists <- Future.sequence(customers.map {
      customer => customer.ask(GetShoppingList).mapTo[ShoppingList]
    })
  } yield shoppingLists

  //print out the shopping lists for the various customers
  val finalF = for {
    shoppingLists <- shoppingListsF
    _ <- Future {
      shoppingLists.foreach {
        shoppingList =>
          val shoppingStr = shoppingList.map {
            case (count, item) => s"    Count: $count Name: ${item.name}"
          }.mkString("\n")
          println(s"Found shopping list:\n$shoppingStr")
      }
    }
    goShoppingRes <- Future.sequence(customers.map { customer =>
      customer.ask(GoShopping).mapTo[String]
    })
    complaints <- {
      (storeManager ? GetComplaints).mapTo[List[String]]
    }
    _ <- Future {
      val complaintStrs = complaints.map(str => s"    Complaint: $str")
      println(s"Complaint count: ${complaints.size}")
      println(complaintStrs.mkString("\n"))
    }
    salesReport <- (storeManager ? GetSalesReport).mapTo[SalesReport]
    _ <- Future {
      val report =
        s"""Sales Report:
            |   Total Revenue: ${salesReport.totalRevenue}
            |   Total Units Sold: ${salesReport.totalUnitsSold}
            |   Total Missed Revenue: ${salesReport.missedRevenue}
            |   Total Missed Units: ${salesReport.missedUnits}
            |   Unit Efficiency: ${salesReport.unitEfficiency}
            |   Sales Efficiency: ${salesReport.salesEfficiency}
         """.stripMargin
      println(report)
    }
  } yield (shoppingLists, goShoppingRes, complaints)

  //shutdown the system and log any errors that occurred
  finalF.onComplete {
    case Success(s) => system.shutdown()
    case Failure(t) =>
      println("An error occurred while running Notkea!")
      t.printStackTrace()
      system.shutdown()
  }

  system.awaitTermination()

  private def propsForActor(name: String)(implicit system: ActorSystem): Props = {
    GuiceAkkaExtension(system).props(name)
  }
}
